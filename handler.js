'use strict';
const nanoid                       = require('nanoid'),
	  {success, error}             = require('./lib/helpers/response'),
	  {getBodyParam, getPathParam} = require('./lib/helpers/request'),
	  {dbConnect}                  = require('./lib/drivers/db'),
	  _                            = require('lodash'),
	  ShortenedURL                 = require('./lib/models/ShortenedURL'),
	  validator                    = require('validator');

/**
 * Shorten provided URL and save it to DB
 * @param {object} event
 * @param {object} context
 * @param {function} callback
 * @constructor
 */
module.exports.Shorten = (event, context, callback) => {
	const extended = getBodyParam(event, 'extended');

	// Validate extended URL string
	if (!extended || !validator.isURL(extended))
		error(callback, 'Not a valid URL', 403);

	// Prepare the query and generate short link ID
	const query = {
		$setOnInsert:
			{extended: extended, short: nanoid(8)}
	};

	// Save to DB
	dbConnect().then(() => {
		ShortenedURL.findOneAndUpdate(
			{extended: extended},
			query,
			{new: true, upsert: true})
			.then(url => success(callback, url))
			.catch(err => error(callback, 'Could not save the URL', err.statusCode || 500));
	});
};

/**
 * Extend provided shorten URL
 * @param {object} event
 * @param {object} context
 * @param {function} callback
 * @constructor
 */
module.exports.Extend = (event, context, callback) => {
	const short = getPathParam(event, 'short');

	if (!short)
		error(callback, 'Not a valid request', 403);

	dbConnect().then(() => {
		ShortenedURL.findOne({short: short})
			.then(url => {
				if (_.isNull(url))
					error(callback, 'Not Found', 404);

				success(callback, url)
			})
			.catch(err => error(callback, 'Could not fetch the URL', err.statusCode || 500));
	});
};

/**
 * Test endpoint
 * @param {object} event
 * @param {object} context
 * @param {function} callback
 */
module.exports.Test = (event, context, callback) => {
	const data = {
		message: 'Test lambda function executed successfully!',
		input: event,
	};

	success(callback, data);
};
