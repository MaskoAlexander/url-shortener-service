# URL Shortener Service

[API doc](https://documenter.getpostman.com/view/1775557/S1ERuwBV#0c6d5565-4b76-4116-8833-da0b831507ae)

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/fa64ee0ae0c61c8b46ae#?env%5BShortener%20service%20local%5D=W3sia2V5IjoiYmFzZV91cmwiLCJ2YWx1ZSI6ImxvY2FsaG9zdDozMDAwIiwiZGVzY3JpcHRpb24iOiIiLCJ0eXBlIjoidGV4dCIsImVuYWJsZWQiOnRydWV9LHsia2V5IjoiYXBpX3ZlcnNpb24iLCJ2YWx1ZSI6IjEiLCJkZXNjcmlwdGlvbiI6IiIsInR5cGUiOiJ0ZXh0IiwiZW5hYmxlZCI6dHJ1ZX1d)

## Pre-requisites
| Name | Version |
| ------ | ------ |
| Docker | v 18.09.0^ |
| docker-compose | v 1.23.0^ |

## Installation

Get a copy of the source code

```sh
$ mkdir \var\www\url-shortener-service
$ cd !$
$ git clone https://MaskoAlexander@bitbucket.org/MaskoAlexander/url-shortener-service.git
$ git checkout -b master origin/master
```

Create env gitignored file and fill it with corresponding variables
```sh
$ cp .env.example .env
```

## Running on the local machine

Run Docker container
```sh
$ docker-compose up
```

In a case of any update or unexpected issues rebuild container
```sh
$ docker-compose up --build
```

Verify the deployment by navigating to your server address in your preferred browser.

[http://127.0.0.1:3000/api/v1/test](http://127.0.0.1:3000/api/v1/test)
