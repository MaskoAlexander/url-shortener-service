# Install node
FROM node:8.10

# Set the workdir /var/www/myapp
WORKDIR /app

# Copy the package.json to workdir
COPY package.json ./

# Run npm install - install the npm dependencies
RUN npm install
RUN npm install serverless -g

# Copy application source
COPY . .

EXPOSE 3000

# Start the application
CMD ["sls", "offline", "start"]