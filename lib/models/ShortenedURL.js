const mongoose = require('mongoose');

const ShortenedURLSchema = new mongoose.Schema({
  short: String,
  extended: String
});
// Kind of Singleton for sls local testing
module.exports = mongoose.models.ShortenedURL || mongoose.model('ShortenedURL', ShortenedURLSchema);