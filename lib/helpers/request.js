const _ = require('lodash');

/**
 * Get param from event body
 * @param {object} event
 * @param {string} param
 * @returns {*}
 */
module.exports.getBodyParam = (event, param) => {
	const body = JSON.parse(event.body);
	return _.get(body, param);
};

/**
 * Get param from event path params object
 * @param {object} event
 * @param {string} param
 * @returns {*}
 */
module.exports.getPathParam = (event, param) => {
	return _.get(event.pathParameters, param);
};

