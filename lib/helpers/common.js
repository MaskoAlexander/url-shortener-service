const _ = require('lodash');

/**
 * Get current environment
 * @return {string}
 */
module.exports.getEnv = () => {
    return _.get(process, 'env.name', 'dev');
};

/**
 * Get json config
 * @param {string} name
 * @param {string} path
 * @param {*} defaultValue
 * @return {*}
 */
module.exports.getConfig = (name, path = null, defaultValue = null) => {
    const json = require(`../../json/${name}.json`);
    if (path) {
        return _.get(json, path, defaultValue);
    } else {
        return json;
    }
};