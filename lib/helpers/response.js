const {getConfig, getEnv} = require('./common');

/**
 * Success response
 * @param {function} cb
 * @param {object} data
 * @param {object} data
 * @return {*}
 */
module.exports.success = (cb, data) => {
	return cb(null, {
		statusCode: 200,
		headers: {
			'Content-Type': 'application/json',
			'Access-Control-Allow-Origin': getCORS()
		},
		body: JSON.stringify(data),
		isBase64Encoded: false
	});
};

/**
 * Error response
 * @param {function} cb
 * @param {string} message
 * @param {int} code
 * @returns {*}
 */
module.exports.error = (cb, message, code = 500) => {
	return cb(null, {
		statusCode: code,
		headers: {
			"Content-Type": "application/json",
			"Access-Control-Allow-Origin": getCORS()
		},
		body: JSON.stringify({message: message}),
		isBase64Encoded: false
	});
};

/**
 * Get CORS
 * @return {string}
 */
const getCORS = () => {
	return getConfig('api', `cors.${getEnv()}`, '*');
};