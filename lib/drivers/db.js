require('dotenv').config();

const mongoose = require('mongoose');
mongoose.Promise = global.Promise; // For a Singleton

let isConnected;

/**
 * Singleton DB connect
 * @returns {Promise<void>|Promise<any | void>}
 */
module.exports.dbConnect = () => {
	if (isConnected)
		return Promise.resolve();

	return mongoose.connect(process.env.DATABASE, {useNewUrlParser: true})
		.then(db => {
			isConnected = db.connections[0].readyState;
		})
		.catch(() => console.error('Can\'t connect to the database'));
};